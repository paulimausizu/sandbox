export const setLang = lang => {
	return dispatch => {
		dispatch(
			{
				type: 'LANG',
				payload: lang
			}
		);
	}
};

export const results = results => {
	return dispatch => {
		dispatch(
			{
				type: 'RESULTS',
				payload: results
			}
		);
	}
};
