import { combineReducers } from 'redux';

const lang = (state = 'de', action) => {
	switch (action.type) {
		case 'LANG':
			return action.payload;
		default:
			return state;
	}
};

const results = (state = [], action) => {
	switch (action.type) {
		case 'RESULTS':
			return action.payload;
		default:
			return state;
	}
};

export default combineReducers({
	lang,
	results
});
