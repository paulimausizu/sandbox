import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-footer-column-link-list',
	styleUrl: 'sbb-footer-column-link-list.scss'
})

export class SbbFooterColumnLinkList {

	render() {
		return (
			<ul>
				<slot />
	 		</ul>
		);
	}
}
