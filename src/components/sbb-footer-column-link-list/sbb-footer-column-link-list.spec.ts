import { TestWindow } from '@stencil/core/testing';
import { SbbFooterColumnLinkList } from './sbb-footer-column-link-list';

describe('sbb-footer-column-link-list', () => {
  it('should build', () => {
    expect(new SbbFooterColumnLinkList()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbFooterColumnLinkListElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbFooterColumnLinkList],
        html: '<sbb-footer-column-link-list>' 
          + '</sbb-footer-column-link-list>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
