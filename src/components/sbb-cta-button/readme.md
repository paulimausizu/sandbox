# sbb-cta-button



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute | Description | Type         | Default     |
| --------------- | --------- | ----------- | ------------ | ----------- |
| `clickCallback` | --        |             | `() => void` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
