import {Component, Prop} from '@stencil/core';


@Component({
	tag: 'sbb-footer-columns',
	styleUrl: 'sbb-footer-columns.scss',
	shadow: true
})

export class SbbFooterColumns {

	@Prop() footertitle: string;

	render() {
		return [
			<ul class="footer-columns-list">
				<slot />
			</ul>
		]
	}
}
