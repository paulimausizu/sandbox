import { Component } from '@stencil/core';
import arrowIcon from './sbb-red-trapeze-arrow';

@Component({
	tag: 'sbb-red-trapeze',
	styleUrl: 'sbb-red-trapeze.scss',
	shadow: true
})

export class SbbRedTrapeze {
	render() {
		return (
			<div class='red-trapeze'>
				<span class='background'></span>
				<div class='content'>
					<h2 class='title'>Mailand. Und gleich mittendrin.</h2>
					<p class='text'>
						Mehr erfahren
						<span class='icon' innerHTML={arrowIcon}></span>
					</p>
				</div>
			</div>
		);
	}
}
