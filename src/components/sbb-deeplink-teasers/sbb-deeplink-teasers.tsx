import { Component, Prop } from '@stencil/core';

@Component({
	tag: 'sbb-deeplink-teasers',
	styleUrl: 'sbb-deeplink-teasers.scss',
	shadow: true
})

export class SbbDeeplinkTeasers {
	@Prop() items: string;
	@Prop() titlesLevel: string;

	render() {
		const items = JSON.parse(this.items);

		return (
			<ul class='teasers'>
				{items.map((item) => {
					return (
						<sbb-deeplink-teaser
							image={item.image}
							teaser-title={item.title}
							title-level={this.titlesLevel}
							text={item.text}
							link={item.link}
						>
						</sbb-deeplink-teaser>
					);
				})}
			</ul>
		);
	}
}
