import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-definition-term',
	styleUrl: 'sbb-definition-term.scss',
	shadow: true
})
export class SbbDefinitionTerm {

	render() {
		return (
			<dt>
				<slot />
			</dt>
		);
	}
}
