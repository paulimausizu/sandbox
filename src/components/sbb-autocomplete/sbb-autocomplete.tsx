import { Component, Prop, State, Element } from '@stencil/core';

@Component({
	tag: 'sbb-autocomplete',
	styleUrl: 'sbb-autocomplete.scss',
	shadow: true
})
export class SbbAutocomplete {
	@Element() private element: HTMLElement;

	@Prop() suggestions: string
	@Prop() name: string
	@Prop() placeholder: string
	@Prop() value: string
	@Prop() label: string
	@State() hasFocus: boolean
	@State() isDirty: boolean = true

	componentWillLoad () {
		// We don't need to show the suggestions if the value is initialy set.
		this.isDirty = !this.value
	}

	getSuggestions () {

		try {
			const suggestions = JSON.parse(this.suggestions)
			if (Array.isArray(suggestions)) {
				return suggestions
			}
		} catch {
			throw new Error(
				`
					sbb-autocomplete: suggestions attribute expected as JSON Array,
					e.g. [{"label": "hello", "id": "world"}]
				`
			)
		}
	}

	onFocus = () => {
		this.hasFocus = true
	}

	onBlur = () => {
		this.hasFocus = false
		this.trigger('sbb-autocomplete_blur')
	}

	onInput = (e) => {
		const input = e.target.value
		this.setValue(input)
		this.isDirty = true
		this.triggerInputEvent(input)
	}

	triggerInputEvent (input) {
		this.trigger('sbb-autocomplete_input', {input})
	}

	onSuggestionSelected (selectedSuggestion, e) {
		e.preventDefault()
		this.isDirty = false
		this.setValue(selectedSuggestion.label || selectedSuggestion)
		this.triggerSelectionEvent(selectedSuggestion)
	}

	triggerSelectionEvent (selection) {
		this.trigger('sbb-autocomplete_selection', {selection})
	}

	trigger (eventName: string, detail?: object) {

		var event = new CustomEvent(eventName, {
			detail
		})
		this.element.dispatchEvent(event)
	}

	setValue (value) {
		this.element.setAttribute('value', value)
	}

	render() {
		const suggestions = this.getSuggestions()
		const hasSuggestions = !!suggestions.length
		const showSuggestions = hasSuggestions && this.hasFocus && this.isDirty

		const result = [
			<input type="text"
				name={this.name}
				placeholder={this.placeholder}
				onInput={this.onInput}
				onFocus={this.onFocus}
				onBlur={this.onBlur}
				value={this.value}
			/>
		]

		if (this.label) {
			const input = result[0]
			const className = this.value ? 'visible' : ''
			result[0] = <label>
				<span class={className}>{this.label}</span>
				{input}
			</label>

		}

		if (showSuggestions) {
			result.push(
				<ul>
					{suggestions.map((suggestion) => {
						const onSuggestionSelected = this.onSuggestionSelected.bind(
							this,
							suggestion
						)
						const label = suggestion.label || suggestion
						return <li onMouseDown={onSuggestionSelected}>
							{label}
						</li>
					})}
				</ul>
			)
		}

		return result
	}

}
//
// <div class="mod_textfield">
// 		<div class="mod_label">
// 			<label id="2_label" class="mod_label_text" for="2">
// 				Ort (optional)
//
//
// 			</label>
// 		</div>
// 	<div class="mod_textfield_container">
// 		<input id="2" name="2" value="" type="text" class="mod_textfield_control" placeholder="Zürich" aria-required="false" minlength="2">
//
// 	</div>
// 	<div id="2_error_message" class="mod_error_message" role="alert"><div id="2_error_message_text" class="mod_error_message_text"></div>
// 	</div>
// </div>
