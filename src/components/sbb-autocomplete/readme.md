# sbb-autocomplete



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description | Type     | Default     |
| ------------- | ------------- | ----------- | -------- | ----------- |
| `label`       | `label`       |             | `string` | `undefined` |
| `name`        | `name`        |             | `string` | `undefined` |
| `placeholder` | `placeholder` |             | `string` | `undefined` |
| `suggestions` | `suggestions` |             | `string` | `undefined` |
| `value`       | `value`       |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
