import { Component } from '@stencil/core';

@Component({
	tag: 'sbb-timetable-search',
	styleUrl: 'sbb-timetable-search.scss',
	shadow: true
})
export class SbbTimetableSearch {
	render() {
		return [
			<div><slot name="origin" ></slot></div>,
			<div><slot name="destination" ></slot></div>
		]
	}
}
