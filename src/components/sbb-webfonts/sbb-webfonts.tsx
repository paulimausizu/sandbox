import {Component, Prop} from '@stencil/core';


@Component({
	tag: 'sbb-webfonts'
})
export class SbbWebfonts {

	@Prop({ mutable: true }) config?: string;

	render() {

		if (!this.config) {
			this.config = '[ { "version": "1_6_subset" }, { "styles": [ { "name": "Roman", "include": true, "display": "fallback" }, { "name": "Bold", "include": true, "display": "swap" }, { "name": "Light", "include": true, "display": "swap" }, { "name": "Ultralight", "include": true, "display": "swap" }, { "name": "Thin", "include": true, "display": "swap" } ] } ]';
		}

		const itemsObject = JSON.parse(this.config),
		webFontsVersion = itemsObject[0].version,
		webFontsToAdd = itemsObject[1].styles;

		if (!webFontsVersion) {
			console.log('Define the version number of the webfont, please. Use either 1, 1_6 or 1_6_subset');
			return false;
		}

		return (
			<style>
			{webFontsToAdd.map((webfont) => {
				if (webfont.include) {
					return(
						`@font-face {
							font-family: 'SBBWeb ${webfont.name}';
							src: url('https://cdn.app.sbb.ch/fonts/v${webFontsVersion}/SBBWeb-${webfont.name}.woff2') format('woff2'),
							url('https://cdn.app.sbb.ch/fonts/v${webFontsVersion}/SBBWeb-${webfont.name}.woff') format('woff');
							font-display: ${webfont.display};
						}`
					)
				}
			})}
			</style>
		);

	}
}
