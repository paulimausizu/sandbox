import {Component, Prop} from '@stencil/core';

@Component({
	tag: 'sbb-footer',
	styleUrl: 'sbb-footer.scss',
	shadow: true
})

export class SbbFooter {

	@Prop() footertitle: string;

	render() {
		return [
			<h1 class="visuallyhidden">{this.footertitle}</h1>,
			<slot />
		]
	}
}
