import { Component, Element, Prop } from '@stencil/core';

@Component({
	tag: 'sbb-language-selector',
	styleUrl: 'sbb-language-selector.scss',
	shadow: true
})

export class SbbLanguageSelector {

	@Prop({ mutable: true }) language?: string;
	@Prop() languages: string;

	@Element() private element: HTMLElement;

	changeLanguage = (evt: Event, language: string, link: string) : void =>  {

		console.log(this);

		if (link) {
			return;
		}

		evt.preventDefault();

		this.language = language;

		var event = new CustomEvent('sbb-language-selector_language-switch', {
			detail: {language},
			bubbles: true
		});

		this.element.dispatchEvent(event)
	}

	render() {
		let languages;

		if (!this.languages) {
			languages = [{'name': 'de'}, {'name': 'fr'}, {'name': 'it'}, {'name': 'en'}];
		} else {
			languages = JSON.parse(this.languages);
		}

		return (
			<div class='footer'>
				<ul class='languages__list'>
					{languages.map((lang) => {

						const linkClass = 'languages__link' + (lang.name === this.language ? ' languages__link--active' : '');

						return (
							<li class='languages__item'>
								<a
									onClick={(evt) => {this.changeLanguage(evt, lang.name, lang.link)}}
									class={linkClass}
									href={lang.link ? lang.link : '#'}
								>
									{lang.name}
								</a>
							</li>
						);
					})}
				</ul>
			</div>
		);
	}
}
