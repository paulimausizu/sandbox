import { TestWindow } from '@stencil/core/testing';
import { SbbLanguageSelector } from './sbb-language-selector';

describe('sbb-language-selector', () => {
  it('should build', () => {
    expect(new SbbLanguageSelector()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbLanguageSelectorElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbLanguageSelector],
        html: '<sbb-language-selector>' 
          + '</sbb-language-selector>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
