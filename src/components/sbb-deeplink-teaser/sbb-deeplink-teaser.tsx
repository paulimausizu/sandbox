import { Component, Prop } from '@stencil/core';
import icon from './sbb-deeplink-teaser-icon';

@Component({
	tag: 'sbb-deeplink-teaser',
	styleUrl: 'sbb-deeplink-teaser.scss',
	shadow: true
})
export class SbbDeeplinkTeaser {

	@Prop() image: string;
	@Prop() teaserTitle: string;
	@Prop() titleLevel: number;
	@Prop() text: string;
	@Prop() link: { link: string, title: string};

	render() {
		return (
			<li class='item'>
				<a class='link' href={this.link.link}>
					<div class='image-wrapper'>
						<img class='image' src={this.image} />
					</div>
					<div class='text-wrapper'>
						{h(
							`h${this.titleLevel}`,
							{ className: 'title'},
							this.teaserTitle
						)}
						<div class='text-wrapper-inner'>
							<p class='text'>{this.text}</p>
							<span class='bottom-link'>
								{this.link.title}
								<span class='bottom-link-icon' innerHTML={icon}></span>
							</span>
						</div>
					</div>
				</a>
			</li>
		);
	}
}
