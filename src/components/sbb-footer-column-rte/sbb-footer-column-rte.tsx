import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-footer-column-rte',
	styleUrl: 'sbb-footer-column-rte.scss',
	shadow: true
})

export class SbbFooterColumnRte {

	render() {
		return (
			<section>
				<slot />
			</section>
		);
	}
}
