import { Component, Element } from '@stencil/core';


@Component({
	tag: 'sbb-table-body',
	styleUrl: 'sbb-table-body.scss'
})
export class SbbTableBody {

	@Element() tbody: HTMLTableElement;

	componentDidLoad() {
		this.tbody.setAttribute('role', 'rowgroup');
		this.tbody.style.display = 'table-row-group';
	}

	render() {
		return (
			<slot />
		);
	}
}
