import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-table',
	styleUrl: 'sbb-table.scss',
	shadow: true
})
export class SbbTable {
	render() {
		return (
			<div role='table' aria-describedby='table'>
				<slot />
			</div>
		);
	}
}
