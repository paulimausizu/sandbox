import {Component, Prop} from '@stencil/core';

@Component({
	tag: 'sbb-footer-column-link-list-item',
	styleUrl: 'sbb-footer-column-link-list-item.scss',
	shadow: true
})

export class SbbFooterColumnLinkListItem {

	@Prop() label: string;
	@Prop() href: string;
	@Prop() external: string;

	render() {
		if (this.external) {
			return (
				<li>
					<a href={this.href} target="_blank" rel="external noopener nofollow">
						{this.label}
						<span class="visuallyhidden">Linkziel öffnet sich in einem neuen Fenster</span>
					</a>
				</li>
			)

		} else {
			return (
				<li>
					<a href={this.href}>
						{this.label}
					</a>
				</li>
			)
		}
	}
}
