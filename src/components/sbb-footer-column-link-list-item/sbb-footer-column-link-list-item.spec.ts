import { TestWindow } from '@stencil/core/testing';
import { SbbFooterColumnLinkListItem } from './sbb-footer-column-link-list-item';

describe('sbb-footer-column-link-list-item', () => {
  it('should build', () => {
    expect(new SbbFooterColumnLinkListItem()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbFooterColumnLinkListItemElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbFooterColumnLinkListItem],
        html: '<sbb-footer-column-link-list-item>' 
          + '</sbb-footer-column-link-list-item>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
