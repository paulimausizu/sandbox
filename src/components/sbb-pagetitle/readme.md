# sbb-pagetitle



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute            | Description | Type     | Default     |
| ------------------- | -------------------- | ----------- | -------- | ----------- |
| `additionalClasses` | `additional-classes` |             | `string` | `undefined` |
| `pageTitle`         | `page-title`         |             | `string` | `undefined` |
| `visuallyhidden`    | `visuallyhidden`     |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
