import { Component, Prop } from '@stencil/core';


@Component({
	tag: 'sbb-form',
	styleUrl: 'sbb-form.scss',
	shadow: true
})
export class SbbForm {

	@Prop() action: string;
	@Prop() method: string;

	render() {
		return (
			<form action={this.action} method={this.method}>
				<slot />
			</form>
		);
	}
}
