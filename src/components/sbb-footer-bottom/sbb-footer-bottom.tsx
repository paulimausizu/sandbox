import { Component } from '@stencil/core';


@Component({
	tag: 'sbb-footer-bottom',
	styleUrl: 'sbb-footer-bottom.scss',
	shadow: true
})
export class SbbFooterBottom {
	render() {
		return (
			<div class='sbb-footer-bottom'>
				<slot />
			</div>
		);
	}
}
