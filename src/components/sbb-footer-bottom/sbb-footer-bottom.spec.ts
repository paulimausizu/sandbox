import { TestWindow } from '@stencil/core/testing';
import { SbbFooterBottom } from './sbb-footer-bottom';

describe('sbb-footer-bottom', () => {
  it('should build', () => {
    expect(new SbbFooterBottom()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLSbbFooterBottomElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [SbbFooterBottom],
        html: '<sbb-footer-bottom>' 
          + '</sbb-footer-bottom>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
