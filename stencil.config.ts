import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

export const config: Config = {
  namespace: 'sbb',
  outputTargets:[
    { type: 'dist' },
    { type: 'docs' },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
      baseUrl: '/sandbox/'
    }
  ],
  copy: [
    {
        src: 'pages'
    },
    {
        src: 'assets'
    }
  ],
  plugins: [
    sass({
      injectGlobalPaths: [
          'src/global/mixins.scss',
          'src/global/variables.scss'
        ]
    })
  ]
};
